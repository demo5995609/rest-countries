let cardContent = document.querySelector(".cards");
fetchData();
function fetchData() {
  fetch("https://restcountries.com/v3.1/all")
    .then((response) => {
      return response.json();
    })
    .then((dataJson) => {
      sessionStorage.setItem("jsonRestCountries", JSON.stringify(dataJson));
      countriesCards();
    })
    .catch((err) => {
      console.error(err);
    });
}
function countriesCards() {
  let dataJson = JSON.parse(sessionStorage.getItem("jsonRestCountries"));
  let countriesCards = "";

  dataJson.forEach((data) => {
    countriesCards += `<div class="card">
      <img
        class="card-img-top"
        src="${data.flags.png}"
        alt="Card image cap"
      />
      <div class="card-body">
        <div class="country-name">
            <h1>${data.name.common}</h1>
        </div>
        <div class="raw-data">
            <div class="population d-flex">
                <span class="font-weight-bold">Population: </span>
                <p class="pr-3">${data.population.toLocaleString("en-us")}</p>
            </div>
            <div class="region d-flex">
                <span class="font-weight-bold">Region: </span>
                <p class="pr-3">${data.region}</p>
            </div>
            <div class="capital d-flex">
                <span class="font-weight-bold">Capital: </span>
                <p class="pr-3">${data.capital}</p>
            </div>
        </div>
      </div>
    </div>`;
  });
  cardContent.innerHTML = countriesCards;
}
//Searching functionality
let searchEle = document.getElementById("search");
searchEle.addEventListener("keydown", searchContent);
let preSearch = "";
function searchContent(event) {
  let newSearch = event.target.value;
  preSearch = newSearch;
  let cards = document.getElementsByClassName("card");
  for (let i = 0; i < cards.length; i++) {
    let countryElement = cards[i].getElementsByClassName("country-name")[0];
    let countryName = countryElement.getElementsByTagName("h1")[0].innerText;

    let regionElement = cards[i].getElementsByClassName("region")[0];
    let regionName = regionElement.getElementsByTagName("p")[0].innerText;
    if (
      countryName.toUpperCase().indexOf(newSearch.toUpperCase()) > -1 &&
      (regionName.toUpperCase().indexOf(preSelectedOption.toUpperCase()) > -1 ||
        preSelectedOption.localeCompare("Filter by Region") == 0)
    ) {
      cards[i].style.display = "block";
    } else {
      cards[i].style.display = "none";
    }
  }
}

// Filtering data on basis of Region
let preSelectedOption = "";
function filterRegion() {
  let filterValue = document.getElementById("region-options");
  let newSelectedVal = filterValue.options[filterValue.selectedIndex].text;
  preSelectedOption = newSelectedVal;
  let cards = document.getElementsByClassName("card");
  console.log(cards);
  for (let i = 0; i < cards.length; i++) {
    let regionElement = cards[i].getElementsByClassName("region")[0];
    let regionName = regionElement.getElementsByTagName("p")[0].innerText;

    let countryElement = cards[i].getElementsByClassName("country-name")[0];
    let countryName = countryElement.getElementsByTagName("h1")[0].innerText;
    if (
      (regionName.toUpperCase().indexOf(newSelectedVal.toUpperCase()) > -1 ||
        newSelectedVal.localeCompare("Filter by Region") == 0) &&
      countryName.toUpperCase().indexOf(preSearch.toUpperCase()) > -1
    ) {
      cards[i].style.display = "block";
    } else {
      cards[i].style.display = "none";
    }
  }
}

//Click on country

let index = document.getElementsByClassName("cards");
// console.log(index[0]);
index[0].addEventListener("click", setId);
function setId(event) {
  let country = event.target
    .closest(".card")
    .querySelector(".country-name").innerText;

  // sessionStorage.setItem("id", index);
  window.location.href = `./assets/src/html/detail-country.html?country=${country}`;
}
