let jsonData = JSON.parse(sessionStorage.getItem("jsonRestCountries"));
console.log(jsonData);
let url = window.location.href;
let urlObj = new URL(url);
// console.log(urlObj.search);
searchCountry();
function searchCountry() {
  const countryTitle = new URLSearchParams(urlObj.search);
  let country = countryTitle.get("country");
  jsonData.forEach((data) => {
    if (data.name.common.localeCompare(country) === 0) {
      cardDetails(data);
    }
  });
}
function cardDetails(countryData) {
  let countryImage = countryData.flags.png;
  let countryName = countryData.name.common;
  let nativeName = Object.values(countryData.name.nativeName)[0].official;
  let population = countryData.population;
  let region = countryData.region;
  let subRegion = countryData.subregion;
  let capital = countryData.capital[0];
  let tld = countryData.tld[0];
  let currency = Object.values(countryData.currencies)[0].name;
  let languageArr = Object.values(countryData.languages);

  // Creating String of languages
  let languages = languageArr.join(", ");
  //Get all Borders
  let borderArr = countryData.borders;
  //border buttons
  let buttons = borderButtons(borderArr);

  document.querySelector(".flag-img").src = countryImage;
  document.querySelector(".country-name").innerText = countryName;
  document.querySelector(".native-name-data").innerText = nativeName;
  document.querySelector(".population-data").innerText =
    population.toLocaleString("en-us");
  document.querySelector(".region-data").innerText = region;
  document.querySelector(".sub-region-data").innerText = subRegion;
  document.querySelector(".capital-data").innerText = capital;
  document.querySelector(".tld").innerText = tld;
  document.querySelector(".currencies-data").innerText = currency;
  console.log(document.querySelector(".currencies-data"));
  document.querySelector(".languages-data").innerText = languages;
  // console.log(document.querySelector(".countires"));
  document.querySelector(".countires").innerHTML = buttons;
}

//border buttons
function borderButtons(borderArr) {
  let borderCode = getBorderCode();

  let buttons = "";
  // console.log(typeof borderArr);
  if (borderArr) {
    borderArr.forEach((border) => {
      // console.log(typeof borderCode[border]);
      borderCode[border];
      buttons += `<button type="button" class="country-name-btn btn btn-light mx-2 w-10">
            ${borderCode[border]}
          </button>`;
    });
  }
  return buttons;
}

// border code countries object
function getBorderCode() {
  return (countryCode = jsonData.reduce((acc, element) => {
    let cca3 = element.cca3;
    let country = element.name.common;
    if (!acc.cca3) {
      acc[cca3] = country;
    }

    // console.log(obj);
    return acc;
  }, {}));
}

//click on  border buttons
let countryButton = document.querySelector(".countires");
countryButton.addEventListener("click", clickedButton);
function clickedButton(event) {
  let country = event.target.innerText;
  window.location.href = `?country=${country}`;
  searchCountry();
}
